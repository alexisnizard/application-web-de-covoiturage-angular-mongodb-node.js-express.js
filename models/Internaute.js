const mongoose = require('mongoose');

// Schéma pour un Internaute
const InternauteSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true },
    motDePasse: { type: String, required: true },
    nom: String,
    prenom: String,
    telephone: String,
    numPermis: String,
    numImmatriculation: String
});

module.exports = mongoose.model('Internaute', InternauteSchema);
