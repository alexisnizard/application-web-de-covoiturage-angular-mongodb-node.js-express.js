import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Covoiturage } from './covoiturage.model';
import { AuthService } from '../src/app/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CovoiturageService {
  private API_URL = 'http://localhost:3000/api';

  constructor(private http: HttpClient, private authService: AuthService) { }

  // Proposer un covoiturage
  proposerCovoiturage(data: Covoiturage): Observable<Covoiturage> {
    return this.http.post<Covoiturage>(`${this.API_URL}/covoiturages`, data, {
      headers: this._createAuthorizationHeader(),
    });
  }

  // Récupérer les covoiturages de l'utilisateur
  mesCovoiturages(): Observable<Covoiturage[]> {
    const email = this.authService.decodeToken()?.email;
    return this.http.get<Covoiturage[]>(`${this.API_URL}/covoiturages`, {
      params: { email: email },
      headers: this._createAuthorizationHeader(),
    });
  }

  // Rechercher des covoiturages
  rechercherCovoiturage(criteria: any): Observable<Covoiturage[]> {
    return this.http.post<Covoiturage[]>(`${this.API_URL}/rechercher-covoiturage`, criteria, {
      headers: this._createAuthorizationHeader(),
    });
  }

  // Supprimer un covoiturage
  supprimerCovoiturage(id: string): Observable<any> {
    return this.http.delete(`${this.API_URL}/covoiturages/${id}`);
  }

  // Créer des en-têtes d'autorisation avec le token
  private _createAuthorizationHeader(): HttpHeaders {
    const token = this.authService.getToken();
    return new HttpHeaders().set('Authorization', `Bearer ${token}`);
  }
}
