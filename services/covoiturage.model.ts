export interface Covoiturage {
  _id?: string;
  villeDepart: string;
  villeArrivee: string;
  date: string;
  nbPlaces: number;
}
