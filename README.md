# Application web de covoiturage - Angular MongoDB Node.js Express.js

### Spécifications générales

Création d'une application web de covoiturage où un internaute peut :
- (une fois connecté) proposer un covoiturage en tant que conducteur ;
- rechercher et, après connexion, sélectionner un covoiturage disponible entre deux villes pour une date souhaitée.

**Bonus** : Présentation d'une carte indiquant le trajet entre la ville de départ et la ville d'arrivée.

Cette application web sera créée en utilisant l'architecture **MEAN** (MongoDB, Express, Angular, et Node.js).

### Modélisation et création d'une base de données avec MongoDB

La base de données inclura au moins les trois collections suivantes :

1. **Collection Internautes** avec les champs : 
   `_id`, `email`, `motDePasse`, `nom`, `prenom`, `téléphone`, `numPermis` (si applicable), `numImmatriculation` (si applicable), etc.

2. **Collection Covoiturages** avec les champs :
   `_id`, `villeDépart`, `villeArrivée`, `date` (*), `email` (correspondant à l'`_id` des Internautes pour le conducteur), `nbPlaces` (hors conducteur), etc.

3. **Collection Transports** avec les champs :
   `_id`, `idCovoiturage` (correspondant à l'`_id` des Covoiturages), `email` (correspondant à l'`_id` des Internautes pour la personne transportée).

(*) *La date est un nombre à 6 chiffres sous le format `AAMMJJ`. Par exemple, le 9 mars 2023 est représenté par `230309`, qui est inférieur à `230102` pour le 2 janvier 2023.*

### Fonctionnalités de l'application web

Les internautes auront la possibilité de :

- S'inscrire (fourniture d'un numéro de permis et d'immatriculation si souhaitant proposer un covoiturage).
- Proposer un covoiturage (création d'un nouveau document dans la collection Covoiturages).
- Rechercher un covoiturage, en précisant la ville de départ, la ville d'arrivée, la date, et un prix maximal (par défaut, le prix moyen des trajets entre les deux villes).
- Sélectionner un covoiturage (ce qui entraîne la création d'un nouveau document dans la collection Transports et la modification du document existant dans la collection Covoiturages).

### Exemples d'affichages

Ci-dessous, vous trouverez des exemples d'affichages du site :

<p align="center">
  <img src="exemple1.png" alt="Exemple d'exécution du programme" width="800">
  <br>
  <img src="exemple4.png" alt="Exemple d'exécution du programme" width="800">
 <br>
  <img src="exemple5.png" alt="Exemple d'exécution du programme" width="800">
 <br>
  <img src="exemple2.png" alt="Exemple d'exécution du programme" width="800">
 <br>
  <img src="exemple3.png" alt="Exemple d'exécution du programme" width="800">
</p>

