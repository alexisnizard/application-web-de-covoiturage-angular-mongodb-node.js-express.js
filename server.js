const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());

// Connection à MongoDB
mongoose.connect('mongodb://0.0.0.0:27017/mean_db', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connecté à la base de données.");
});

const Internaute = require('./models/Internaute'); // Assurez-vous que le chemin est correct

// Route pour l'inscription d'un nouvel internaute
app.post('/api/inscription', async (req, res) => {
    try {
        // Vérifier si l'utilisateur existe déjà
        const existingUser = await Internaute.findOne({ email: req.body.email });
        if (existingUser) {
            return res.status(400).send('Un utilisateur avec cet email existe déjà.');
        }

        // Création d'un nouvel utilisateur
        const internaute = new Internaute(req.body); // les données sont prises directement depuis le corps de la requête

        // Enregistrement de l'utilisateur dans la base de données
        const savedInternaute = await internaute.save();

        // Répondre avec le nouvel utilisateur (sans le mot de passe)
        res.status(201).json({ internaute: savedInternaute._id });

    } catch (error) {
        res.status(500).send("Erreur lors de l'enregistrement de l'utilisateur.");
    }
});

const jwt = require('jsonwebtoken');

// Ceci est un exemple simple et ne comprend pas la gestion des sessions ou des tokens
app.post('/api/connexion', async (req, res) => {
  try {
      // Trouver l'utilisateur basé sur l'email
      const internaute = await Internaute.findOne({ email: req.body.email });

      if (!internaute) {
          return res.status(400).send('Aucun compte associé à cet email.');
      }

      // Comparer le mot de passe (pour un vrai projet, utilisez toujours un hashage et jamais de texte brut)
      if (req.body.motDePasse !== internaute.motDePasse) {
          return res.status(400).send('Mot de passe incorrect.');
      }

      const userPayload = { email: internaute.email };
      const accessToken = jwt.sign(userPayload, 'Votre_SECRET_JWT', { expiresIn: '1h' }); // Choisissez un secret plus sûr pour la production!

      res.status(200).json({ token: accessToken, email: internaute.email }); // envoi du token et email dans la réponse

  } catch (error) {
      res.status(500).send("Erreur lors de la tentative de connexion.");
  }
});

const Covoiturage = require('./models/Covoiturage');
// Route pour créer un covoiturage
app.post('/api/covoiturages', async (req, res) => {
  try {
      const covoiturage = new Covoiturage(req.body);
      await covoiturage.save();
      res.status(201).json(covoiturage);
  } catch (error) {
      console.error(error); // Imprime l'erreur dans la console du serveur
      res.status(500).send("Erreur lors de la création du covoiturage: " + error.message);
  }
});



// Route pour obtenir les covoiturages d'un utilisateur
app.get('/api/covoiturages', async (req, res) => {
  try {
      const email = req.query.email; // ou utilisez le décodage de token pour obtenir l'email de l'utilisateur authentifié
      const covoiturages = await Covoiturage.find({ email: email });
      res.status(200).json(covoiturages);
  } catch (error) {
      res.status(500).send("Erreur lors de la récupération des covoiturages.");
  }
});


// Route pour supprimer un covoiturage
app.delete('/api/covoiturages/:id', async (req, res) => {
  try {
      const covoiturage = await Covoiturage.findByIdAndRemove(req.params.id);
      if (!covoiturage) res.status(404).send({ message: "Aucun covoiturage trouvé." });
      else res.status(200).json({ message: "Covoiturage supprimé avec succès." });
  } catch (error) {
      res.status(500).send({ message: "Erreur lors de la suppression du covoiturage.", error: error.message });
  }
});


// Route pour rechercher des covoiturages
app.post('/api/rechercher-covoiturage', async (req, res) => {
  try {
    const { villeDepart, villeArrivee, date } = req.body;

    // Utilisation de find avec des filtres pour les critères de recherche
    const covoiturages = await Covoiturage.find({
      villeDepart: villeDepart,
      villeArrivee: villeArrivee,
      date: date
    });

    res.status(200).json(covoiturages);
  } catch (error) {
    console.error(error);
    res.status(500).send('Erreur lors de la recherche des covoiturages.');
  }
});

// Démarrer le serveur
const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Serveur en écoute sur le port ${PORT}`);
});
