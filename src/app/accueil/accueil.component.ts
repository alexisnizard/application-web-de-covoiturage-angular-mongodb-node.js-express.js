// accueil.component.ts
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { CovoiturageService } from 'services/covoiturage.service'; // Ajustez le chemin si nécessaire

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  userEmail: string | null = null;
  covoiturages: any[] = []; // Tableau pour stocker les covoiturages de l'utilisateur

  constructor(private authService: AuthService, private covoiturageService: CovoiturageService) { }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      const tokenInfo = this.authService.decodeToken();
      this.userEmail = tokenInfo?.email || null;

      // Si l'utilisateur est connecté, récupérer ses covoiturages
      this.covoiturageService.mesCovoiturages().subscribe(
        data => {
          this.covoiturages = data; // Assigner les covoiturages récupérés au tableau
        },
        error => {
          console.error('Erreur lors de la récupération des covoiturages', error);
        }
      );
    }
  }

  // Vérifie si l'utilisateur est connecté
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  // Gère la déconnexion de l'utilisateur
  onLogout() {
    this.authService.logout();
    // La logique supplémentaire pour gérer la déconnexion peut être ajoutée ici
  }
}
