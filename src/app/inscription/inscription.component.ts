import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  user = {
    email: '',
    motDePasse: ''
  };

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    // URL de l'endpoint d'inscription
    const url = 'http://localhost:3000/api/inscription';

    this.http.post(url, this.user).subscribe(
      res => {
        console.log(res);
        alert('Inscription réussie!');
      },
      err => {
        console.error(err);
        alert('Erreur lors de l\'inscription. Veuillez réessayer.');
      }
    );
  }
}
