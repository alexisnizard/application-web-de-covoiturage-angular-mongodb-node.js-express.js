import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AuthService {
  // Sauvegarde le token JWT dans le stockage local
  saveToken(token: string) {
    localStorage.setItem('userToken', token);
  }

  // Récupère le token JWT du stockage local
  getToken() {
    return localStorage.getItem('userToken');
  }

  // Vérifie si l'utilisateur est connecté
  isLoggedIn() {
    const token = this.getToken();
    // Vérifie si le token existe (vous pouvez ajouter des vérifications supplémentaires ici)
    return token != null;
  }

  // Décode le token JWT et renvoie le Payload
  decodeToken() {
    const token = this.getToken();
    if (!token) { return null; }

    // Le token est au format 'Header.Payload.Signature'. Nous voulons le Payload.
    const segments = token.split('.');
    if (segments.length !== 3) {
      throw new Error('Structure du token invalide');
    }

    // Le Payload est encodé en Base64Url. Décodez-le.
    const base64 = segments[1].replace(/-/g, '+').replace(/_/g, '/');
    const payload = JSON.parse(this.decodeBase64(base64));

    return payload; // Ceci est un objet JSON représentant le Payload du token.
  }

  // Décode une chaîne en Base64
  private decodeBase64(str: string) {
    return decodeURIComponent(atob(str).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  }

  // Déconnecte l'utilisateur
  logout() {
    localStorage.removeItem('userToken');
  }

  // Obtient les informations de l'utilisateur actuellement connecté
  getCurrentUser() {
    const tokenPayload = this.decodeToken();
    if (!tokenPayload) {
      return null;
    }

    return {
      email: tokenPayload.email, // Le nom de la propriété doit correspondre à ce que votre backend a défini
    };
  }
}
