import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesCovoituragesComponent } from './mes-covoiturages.component';

describe('MesCovoituragesComponent', () => {
  let component: MesCovoituragesComponent;
  let fixture: ComponentFixture<MesCovoituragesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MesCovoituragesComponent]
    });
    fixture = TestBed.createComponent(MesCovoituragesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
