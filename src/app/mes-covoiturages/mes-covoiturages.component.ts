import { Component, OnInit } from '@angular/core';
import { CovoiturageService } from 'services/covoiturage.service';
import { AuthService } from '../auth/auth.service';
import { Covoiturage } from '../../../services/covoiturage.model';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'app-mes-covoiturages',
  templateUrl: './mes-covoiturages.component.html',
  styleUrls: ['./mes-covoiturages.component.css']
})
export class MesCovoituragesComponent implements OnInit {
  faTimes = faTimes; // Icône pour la suppression des covoiturages
  covoiturages: Covoiturage[] = []; // Tableau pour stocker les covoiturages

  constructor(
    private covoiturageService: CovoiturageService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.chargerCovoiturages(); // Chargement initial des covoiturages
  }

  chargerCovoiturages() {
    if (this.authService.isLoggedIn()) {
      this.covoiturageService.mesCovoiturages().subscribe(
        data => {
          this.covoiturages = data; // Mise à jour de la liste des covoiturages
        },
        error => {
          console.error('Erreur lors de la récupération des covoiturages', error);
        }
      );
    }
  }

  supprimerCovoiturage(id?: string) {
    if (id) {
      this.covoiturageService.supprimerCovoiturage(id).subscribe(
        response => {
          console.log(response.message);

          this.chargerCovoiturages();
        },
        error => {
          console.error('Erreur lors de la suppression du covoiturage!', error);

        }
      );
    }
  }
}
