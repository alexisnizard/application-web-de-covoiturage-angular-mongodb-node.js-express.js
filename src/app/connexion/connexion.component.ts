import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent {
  user = {
    email: '',
    motDePasse: ''
  };

  constructor(private http: HttpClient, private router: Router) {} // injection de Router

  onSubmit() {
    this.http.post<any>('http://localhost:3000/api/connexion', this.user).subscribe(
      response => {
        console.log(response); // la réponse doit contenir le JWT
        localStorage.setItem('userToken', response.token); // stockage du token
        localStorage.setItem('userEmail', response.email); // stockage de l'email de l'utilisateur
        this.router.navigate(['/']); // redirection vers la page d'accueil
      },
      error => {
        console.error('Il y a eu une erreur!', error);
      }
    );
  }
}
