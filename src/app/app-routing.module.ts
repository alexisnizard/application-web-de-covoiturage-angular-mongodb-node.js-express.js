import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { ProposerCovoiturageComponent } from './proposer-covoiturage/proposer-covoiturage.component';
import { RechercherCovoiturageComponent } from './rechercher-covoiturage/rechercher-covoiturage.component';
import { MesCovoituragesComponent } from './mes-covoiturages/mes-covoiturages.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'proposer-covoiturage', component: ProposerCovoiturageComponent },
  { path: 'rechercher-covoiturage', component: RechercherCovoiturageComponent },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'mes-covoiturages', component: MesCovoituragesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
