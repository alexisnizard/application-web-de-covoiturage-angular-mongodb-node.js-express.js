import { Component } from '@angular/core';

import { CovoiturageService } from 'services/covoiturage.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-proposer-covoiturage',
  templateUrl: './proposer-covoiturage.component.html',
  styleUrls: ['./proposer-covoiturage.component.css']
})
export class ProposerCovoiturageComponent {
  covoiturage = {
    villeDepart: '',
    villeArrivee: '',
    date: '',
    nbPlaces: 0,
    email: ''
  };

  constructor(private covoiturageService: CovoiturageService,private authService: AuthService) {

  const currentUser = this.authService.getCurrentUser();
  if (currentUser) {
    this.covoiturage.email = currentUser.email;
  }
}

  onSubmit() {
    this.covoiturageService.proposerCovoiturage(this.covoiturage).subscribe(
      response => {
        console.log('Covoiturage proposé avec succès', response);
      },
      error => {
        console.error('Erreur lors de la proposition du covoiturage', error);
      }
    );
  }
}
