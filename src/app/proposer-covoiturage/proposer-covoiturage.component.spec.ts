import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposerCovoiturageComponent } from './proposer-covoiturage.component';

describe('ProposerCovoiturageComponent', () => {
  let component: ProposerCovoiturageComponent;
  let fixture: ComponentFixture<ProposerCovoiturageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProposerCovoiturageComponent]
    });
    fixture = TestBed.createComponent(ProposerCovoiturageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
