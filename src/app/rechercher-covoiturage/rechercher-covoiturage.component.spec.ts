import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RechercherCovoiturageComponent } from './rechercher-covoiturage.component';

describe('RechercherCovoiturageComponent', () => {
  let component: RechercherCovoiturageComponent;
  let fixture: ComponentFixture<RechercherCovoiturageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RechercherCovoiturageComponent]
    });
    fixture = TestBed.createComponent(RechercherCovoiturageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
