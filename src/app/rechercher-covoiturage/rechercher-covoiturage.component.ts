import { Component } from '@angular/core';
import { CovoiturageService } from 'services/covoiturage.service';
import { Covoiturage } from 'services/covoiturage.model';

@Component({
  selector: 'app-rechercher-covoiturage',
  templateUrl: './rechercher-covoiturage.component.html',
  styleUrls: ['./rechercher-covoiturage.component.css']
})
export class RechercherCovoiturageComponent {
  searchCriteria = {
    villeDepart: '',
    villeArrivee: '',
    date: ''
  };

  covoiturages: Covoiturage[] = [];

  constructor(private covoiturageService: CovoiturageService) {}

  onSearchCovoiturage() {
    this.covoiturageService.rechercherCovoiturage(this.searchCriteria).subscribe(
      data => {
        this.covoiturages = data; // les covoiturages qui correspondent aux critères de recherche
      },
      error => {
        console.error('Il y a eu une erreur lors de la récupération des covoiturages!', error);
      }
    );
  }
}
